package io.androidpro.domain

/**
 * Created by jaroslawmichalik on 02/10/2023
 */
data class Order(
    val orderId: String, // Unique identifier for the order
    val customerName: String, // Name of the customer who placed the order
    val customerAddress: Address, // Address where the order needs to be delivered
    val customerContact: String, // Contact details of the customer
    val restaurantName: String, // Name of the restaurant where the order is placed
    val restaurantAddress: Address, // Address of the restaurant
    val orderItems: List<OrderItem>, // List of items in the order
    val specialInstructions: String?, // Any special instructions provided by the customer
    var orderStatus: OrderStatus, // Current status of the order
    val timestamp: Long // Time when the order was placed
)

data class Address(val latitude: Double, val longitude: Double, val address: String)

data class OrderItem(
    val itemName: String, // Name of the item
    val quantity: Int, // Quantity of the item ordered
    val price: Double // Price of the item
)

enum class OrderStatus {
    PLACED,
    ACCEPTED,
    EN_ROUTE,
    DELIVERED,
    CANCELLED
}
