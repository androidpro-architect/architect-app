package io.androidpro.domain

sealed class GpsUpdate {
    data class GpsData(val latitude: Double, val longitude: Double) : GpsUpdate()
    data class Error(val message: String) : GpsUpdate()
}