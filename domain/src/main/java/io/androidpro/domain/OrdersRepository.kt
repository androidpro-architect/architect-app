package io.androidpro.domain

interface OrdersRepository {

    suspend fun getAll(): List<Order>
    suspend fun getById(id: String): Order
}

