package io.androidpro.architect.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.core.content.ContextCompat
import io.androidpro.domain.GpsDataProvider
import io.androidpro.domain.GpsUpdate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class GpsProvider(private val context: Context): GpsDataProvider {

    private val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    private val _gpsUpdates = MutableSharedFlow<GpsUpdate>()

    override fun listenToGpsUpdates() = _gpsUpdates.asSharedFlow()
        .onStart { startListening() }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            CoroutineScope(Dispatchers.Main).launch {
                _gpsUpdates.emit(GpsUpdate.GpsData(location.latitude, location.longitude))
            }
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    @SuppressLint("MissingPermission")
    fun startListening() {
        if (isLocationPermissionGranted()) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f, locationListener)
        } else {
            CoroutineScope(Dispatchers.Main).launch {
                _gpsUpdates.emit(GpsUpdate.Error("Permission required"))
            }
        }
    }

    fun stopListening() {
        locationManager.removeUpdates(locationListener)
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }


}